package com.example.manu.contactsinviter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.manu.contactsinviter.R;
import android.content.ContentResolver;
import android.widget.EditText;

import static android.view.View.OnClickListener;

public class EventList extends Activity{

    //@Override
    private void addClick(){
       // Intent intent = new Intent(getBaseContext(), EventList.class);
        startActivity(new Intent(".EventName"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);
        //EditText name = (EditText)findViewById(R.id.eventname);
        final Button button = (Button) findViewById(R.id.add);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v){
                //Button add = (Button)findViewById(R.id.add);
                switch (v.getId()){
                    case R.id.add:
                        addClick();
                        break;
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
