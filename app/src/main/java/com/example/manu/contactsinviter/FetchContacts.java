package com.example.manu.contactsinviter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.provider.ContactsContract;
import android.content.ContentResolver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FetchContacts extends Activity {

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_contacts);
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        List names = new ArrayList<String>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_contacts);
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                //String id = cur.getString(
                //        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                //TextView temp = new TextView(this);
                //temp.setText(name);

                //EditText temp = (EditText) findViewById(R.id.contactname);
                //temp.setText(name);
                //System.out.print(name);
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    //System.out.print(name);
                    names.add(name);
                }
                //return names;
            }
            String[] arrname = new String[names.size()];
            names.toArray(arrname);
            //Toast.makeText(this,dsf[1],Toast.LENGTH_SHORT);
            Arrays.sort(arrname);
            ListView tem = (ListView) findViewById(R.id.contactnames);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                    android.R.id.text1, arrname);
            tem.setAdapter(adapter);
        }
    }

    /*class MySalaryComp implements Comparator<ArrayList<String>> {

        @Override
        public int compare(ArrayList<String> e1, ArrayList<String> e2) {
            return e1.get(1).compareTo(e2.get(1));
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fetch_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /*public static Map<String> getPhonebook(Context context)
    {
        Map<String> result = new HashMap<String>();
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
        while(cursor.moveToNext())
        {
            int name_id = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String name = cursor.getString(name_id);
            result.put(name);
        }
        return result;
    }*/
}
